//
//  allView.m
//  iClCUlator_test
//
//  Created by Liu_zc on 2019/7/19.
//  Copyright © 2019 Liu_zc. All rights reserved.
//

#import "allView.h"
#import <SDAutoLayout/SDAutoLayout.h>

#define RGB(r, g, b) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:1.f]
#define Width    [UIScreen mainScreen].bounds.size.width
#define Height [UIScreen mainScreen].bounds.size.height


@interface allView ()
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UILabel *resultLabel;

@end


@implementation allView

//- (void) loadThem {
//    [self loadResult];
//    [self loadEquation];
//}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self loadResult];
    [self loadEquation];
}

- (void) loadResult {
    self.resultLabel = [UILabel new];
    [self addSubview:self.resultLabel];
    self.resultLabel.sd_layout.bottomSpaceToView(self.collectionView, 15).widthIs(Width - 40).heightIs(Height * 0.1116).centerXIs(Width/2);
    
    self.resultLabel.backgroundColor = RGB(67, 99, 77);
    self.resultLabel.textAlignment = NSTextAlignmentRight;
    self.resultLabel.font = [UIFont fontWithName:@"PingFangTC-Light" size:Height * 0.1];
    //    PingFangTC-Thin
    self.resultLabel.text = @"0";
}

- (void) loadEquation {
    self.resultLabel = [UILabel new];
    [self addSubview:self.resultLabel];
    self.resultLabel.sd_layout.bottomSpaceToView(self.collectionView, Height * 0.1116 + 15 + Height * 0.0279).leftSpaceToView(self, 0).widthIs(Width).heightIs(Height * 0.1339);
    
    self.resultLabel.backgroundColor = RGB(99, 67, 77);
    self.resultLabel.textAlignment = NSTextAlignmentRight;
}



@end
